/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package svt;

import java.awt.event.KeyEvent;
import java.awt.Toolkit;
import java.awt.Dimension;
import javax.swing.table.DefaultTableModel;
import java.util.Vector;
import java.io.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JFileChooser;

/**
 *
 * @author the-scrabi
 */
public class EditDialog extends javax.swing.JFrame {

    /**
     * Creates new form EditDialog
     */
    public Vector<String> english = new Vector<>();
    public Vector<String> deutsch = new Vector<>();
    private File file = null;
    private JFileChooser fileDialog = new JFileChooser();
    FileNameExtensionFilter filter = new FileNameExtensionFilter("SchabiVocFile SVF", "svf");

    public EditDialog() {
        initComponents();
        
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width/4, dim.height/2);
        
        rowSpinner.setValue(1);
        saveButton.setEnabled(false);
    }

    public void newValue() {
        file = null;
        english = new Vector<>();
        deutsch = new Vector<>();
        rowSpinner.setValue(1);
        DefaultTableModel model = (DefaultTableModel) vocTable.getModel();
        model.setRowCount(1);
        model.setValueAt(null, 0, 0);
        model.setValueAt(null, 0, 1);
    }

    public boolean hasValue() {
        DefaultTableModel model = (DefaultTableModel) vocTable.getModel();
        if (model.getValueAt(0, 0) == null) {
            return false;
        } else {
            return true;
        }
    }

    public void setValue(Vector<String> e, Vector<String> d) {
        english = e;
        deutsch = d;
        rowSpinner.setValue(e.size());
        DefaultTableModel model = (DefaultTableModel) vocTable.getModel();
        model.setRowCount(e.size());
        for (int i = 0; i < e.size(); i++) {
            model.setValueAt(e.get(i), i, 0);
            model.setValueAt(d.get(i), i, 1);
        }
    }

    public void save() {
        System.out.println("Save Into: " + file.getAbsolutePath());
        try {
            FileOutputStream out = new FileOutputStream(file.getAbsolutePath());
            for (int i = 0; i < english.size(); i++) {
                out.write((english.get(i) + ";"
                        + deutsch.get(i) + "\n").getBytes());
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveAs() {
        fileDialog.setFileFilter(filter);
        fileDialog.setDialogTitle("Save Voc List");
        fileDialog.showSaveDialog(this);

        file = fileDialog.getSelectedFile();
        if (file == null) {
            return;
        }
        if (!file.getName().contains(".svf")) {
            file = new File(file.getAbsolutePath() + ".svf");
        }
        save();
    }

    public void open() {
        fileDialog.setFileFilter(filter);
        fileDialog.setDialogTitle("Load Voc List");
        fileDialog.showOpenDialog(this);

        file = fileDialog.getSelectedFile();
        if (file == null) {
            System.out.println("File: " + file.getAbsolutePath() + " couldn't be opened.");
            return;
        }
        load();
    }

    public void commandLoad(String f) {
        file = new File(f);
        if (file == null) {
            System.out.println("File: " + file.getAbsolutePath() + " couldn't be opened.");
            return;
        }
        load();
    }

    private void load() {
        System.out.println("Load File: " + file.getAbsolutePath());
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            new BufferedInputStream(
                                    new FileInputStream(file))));
            String buffer = null;
            String[] abuff = null;

            Vector<String> e = new Vector<>();
            Vector<String> d = new Vector<>();
            while ((buffer = in.readLine()) != null) {
                abuff = buffer.split(";");
                e.add(abuff[0]);
                d.add(abuff[1]);
            }
            setValue(e, d);
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean tableIsOk() {
        DefaultTableModel model = (DefaultTableModel) vocTable.getModel();
        boolean listIsOk = true;
        for (int i = 0; i < vocTable.getRowCount(); i++) {
            if (model.getValueAt(i, 0) == null) {
                listIsOk = false;
            }
            if (model.getValueAt(i, 1) == null) {
                listIsOk = false;
            }
        }
        return listIsOk;
    }
    
    private void syncTable() {
        DefaultTableModel model = (DefaultTableModel) vocTable.getModel();
        if (tableIsOk()) {
            english = new Vector<>();
            deutsch = new Vector<>();
            for (int i = 0; i < vocTable.getRowCount(); i++) {
                english.add((String) model.getValueAt(i, 0));
                deutsch.add((String) model.getValueAt(i, 1));
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        vocTable = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        okButton = new javax.swing.JButton();
        rowSpinner = new javax.swing.JSpinner();
        saveButton = new javax.swing.JButton();

        vocTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "English", "Deutsch"
            }
        ));
        vocTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                vocTableMouseClicked(evt);
            }
        });
        vocTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                vocTableKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(vocTable);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel1.setPreferredSize(new java.awt.Dimension(400, 50));

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        rowSpinner.setPreferredSize(new java.awt.Dimension(100, 28));
        rowSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                rowSpinnerStateChanged(evt);
            }
        });
        rowSpinner.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                rowSpinnerPropertyChange(evt);
            }
        });

        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rowSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 90, Short.MAX_VALUE)
                .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(okButton)
                    .addComponent(rowSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(saveButton))
                .addContainerGap())
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        DefaultTableModel model = (DefaultTableModel) vocTable.getModel();
        if (tableIsOk()) {
            syncTable();
            this.setVisible(false);
        }
    }//GEN-LAST:event_okButtonActionPerformed

    private void vocTableKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_vocTableKeyTyped
        saveButton.setEnabled(tableIsOk());
    }//GEN-LAST:event_vocTableKeyTyped

    private void rowSpinnerPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_rowSpinnerPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_rowSpinnerPropertyChange

    private void rowSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_rowSpinnerStateChanged
        if ((int) rowSpinner.getValue() < 1) {
            rowSpinner.setValue(1);
        }
        if ((int) rowSpinner.getValue() > 500) {
            rowSpinner.setValue(500);
        }

        DefaultTableModel model = (DefaultTableModel) vocTable.getModel();
        model.setRowCount((int) rowSpinner.getValue());
        
        saveButton.setEnabled(tableIsOk());
    }//GEN-LAST:event_rowSpinnerStateChanged

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        if(!tableIsOk())
            return;
        else
            syncTable();
        if (file != null) {
            save();
        } else {
            saveAs();
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void vocTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_vocTableMouseClicked
        saveButton.setEnabled(tableIsOk());
    }//GEN-LAST:event_vocTableMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EditDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EditDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EditDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EditDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditDialog().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton okButton;
    private javax.swing.JSpinner rowSpinner;
    private javax.swing.JButton saveButton;
    private javax.swing.JTable vocTable;
    // End of variables declaration//GEN-END:variables
}
